# see the URL below for information on how to write OpenStudio measures
# http://nrel.github.io/OpenStudio-user-documentation/reference/measure_writing_guide/

# start the measure
class BoilerFuelSwitch < OpenStudio::Measure::ModelMeasure

  # require all .rb files in resources folder
  Dir[File.dirname(__FILE__) + '/resources/*.rb'].each { |file| require file }

  # resource file modules
  include OsLib_HelperMethods

  # human readable name
  def name
    # Measure name should be the title case of the class name.
    return 'Boiler Fuel Switch'
  end

  # human readable description
  def description
    return 'Simple measure that loops through boilers and changes the fuel type.'
  end

  # human readable description of modeling approach
  def modeler_description
    return 'If requested fuel type for all boilers is the same as the existing fuel type, then it will result in NA'
  end

  # define the arguments that the user will input
  def arguments(model)
    args = OpenStudio::Measure::OSArgumentVector.new

    # the name of the space to add to the model
    boiler_fuel_type = OpenStudio::Measure::OSArgument.makeStringArgument('boiler_fuel_type', true)
    boiler_fuel_type.setDisplayName('Boiler Fuel Type')
    boiler_fuel_type.setDescription('This sting, if valid will be set as the fuel type for all boilers in the model.')
    args << boiler_fuel_type

    # make an argument for use_upstream_args
    use_upstream_args = OpenStudio::Measure::OSArgument.makeBoolArgument('use_upstream_args', true)
    use_upstream_args.setDisplayName('Use Upstream Argument Values')
    use_upstream_args.setDescription('When true this will look for arguments or registerValues in upstream measures that match arguments from this measure, and will use the value from the upstream measure in place of what is entered for this measure.')
    use_upstream_args.setDefaultValue(true)
    args << use_upstream_args

    return args
  end

  # define what happens when the measure is run
  def run(model, runner, user_arguments)
    super(model, runner, user_arguments)

    # assign the user inputs to variables
    args = OsLib_HelperMethods.createRunVariables(runner, model, user_arguments, arguments(model))
    if !args then return false end

    # lookup and replace argument values from upstream measures
    if args['use_upstream_args'] == true
      args.each do |arg,value|
        next if arg == 'use_upstream_args' # this argument should not be changed
        value_from_osw = OsLib_HelperMethods.check_upstream_measure_for_arg(runner, arg)
        if !value_from_osw.empty?
          runner.registerInfo("Replacing argument named #{arg} from current measure with a value of #{value_from_osw[:value]} from #{value_from_osw[:measure_name]}.")
          new_val = value_from_osw[:value]
          # todo - make code to handle non strings more robust. check_upstream_measure_for_arg coudl pass bakc the argument type
          if arg == 'total_bldg_floor_area'
            args[arg] = new_val.to_f
          elsif arg == 'num_stories_above_grade'
            args[arg] = new_val.to_f
          elsif arg == 'zipcode'
            args[arg] = new_val.to_i
          else
            args[arg] = new_val
          end
        end
      end
    end

    # variable for initial and final fuel types across components
    fuel_types = []
    fuel_types_final = []

    # alter boiler_fuel_type per user argument
    if args['boiler_fuel_type'].empty?
      runner.registerInfo("No fuel type was entered for boilers. Boiler fuel types will not be altered.")
    else
      model.getBoilerHotWaters.each do |boiler|
        fuel_types << boiler.fuelType
        is_valid = boiler.setFuelType(args['boiler_fuel_type'])
        if not is_valid
          runner.registerWarning("Could not set fuel type for #{boiler.name} to #{args['boiler_fuel_type']}")
        else
          fuel_types_final << boiler.fuelType
        end
      end
    end

    # report initial condition of model
    runner.registerInitialCondition("The building started with fuel type #{fuel_types.uniq.sort.join(",")} for selected component types.")

    # report final condition of model
    runner.registerFinalCondition("The building finished with boilers of fuel type #{fuel_types_final.uniq.sort.join(",")} for selected component types.")

    if fuel_types_final.size == 0 || fuel_types.uniq.sort == fuel_types_final.uniq.sort
      runner.registerAsNotApplicable("No changes were made to the fuel types for selected component types.")
    end

    return true
  end
end

# register the measure to be used by the application
BoilerFuelSwitch.new.registerWithApplication
