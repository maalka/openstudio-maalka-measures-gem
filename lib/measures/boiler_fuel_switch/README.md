

###### (Automatically generated documentation)

# Boiler Fuel Switch

## Description
Simple measure that loops through boilers and changes the fuel type.

## Modeler Description
If requested fuel type for all boilers is the same as the existing fuel type, then it will result in NA

## Measure Type
ModelMeasure

## Taxonomy


## Arguments


### Boiler Fuel Type
This sting, if valid will be set as the fuel type for all boilers in the model.
**Name:** boiler_fuel_type,
**Type:** String,
**Units:** ,
**Required:** true,
**Model Dependent:** false

### Use Upstream Argument Values
When true this will look for arguments or registerValues in upstream measures that match arguments from this measure, and will use the value from the upstream measure in place of what is entered for this measure.
**Name:** use_upstream_args,
**Type:** Boolean,
**Units:** ,
**Required:** true,
**Model Dependent:** false




