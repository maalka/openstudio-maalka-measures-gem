

###### (Automatically generated documentation)

# Maalka Formatted Monthly JSON Utility Data

## Description
Maalka Formatted Monthly JSON Utility Data

## Modeler Description
Add Maalka Formatted Monthly JSON Utility Data to OSM as a UtilityBill Object

## Measure Type
ModelMeasure

## Taxonomy


## Arguments


### JSON file name
Name of the JSON file to import data from. This is the filename with the extension (e.g. NewWeather.epw). Optionally this can inclucde the full file path, but for most use cases should just be file name.
**Name:** json,
**Type:** String,
**Units:** ,
**Required:** true,
**Model Dependent:** false

### Start date
Start date format %Y%m%dT%H%M%S with Hour Min Sec optional
**Name:** start_date,
**Type:** String,
**Units:** ,
**Required:** true,
**Model Dependent:** false

### End date
End date format %Y%m%dT%H%M%S with Hour Min Sec optional
**Name:** end_date,
**Type:** String,
**Units:** ,
**Required:** true,
**Model Dependent:** false

### remove all existing Utility Bill data objects from model
remove all existing Utility Bill data objects from model
**Name:** remove_existing_data,
**Type:** Boolean,
**Units:** ,
**Required:** true,
**Model Dependent:** false

### Set RunPeriod Object in model to use start and end dates
Set RunPeriod Object in model to use start and end dates.  Only needed once if multiple copies of measure being used.
**Name:** set_runperiod,
**Type:** Boolean,
**Units:** ,
**Required:** true,
**Model Dependent:** false




