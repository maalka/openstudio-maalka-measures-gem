require 'openstudio'
require 'openstudio/ruleset/ShowRunnerOutput'

require "#{File.dirname(__FILE__)}/../measure.rb"

require 'minitest/autorun'

class MaalkaMonthlyJsonUtilityData_Test < MiniTest::Test
  # method to apply arguments, run measure, and assert results (only populate args hash with non-default argument values)
  def apply_measure_to_model(test_name, args, model_name = nil, result_value = 'Success', _warnings_count = 0, info_count = nil)
    # create an instance of the measure
    measure = MaalkaMonthlyJsonUtilityData.new

    # create an instance of a runner with OSW
    osw_path = OpenStudio::Path.new(File.dirname(__FILE__) + '/test.osw')
    osw = OpenStudio::WorkflowJSON.load(osw_path).get
    runner = OpenStudio::Measure::OSRunner.new(osw)

    if model_name.nil?
      # make an empty model
      model = OpenStudio::Model::Model.new
    else
      # load the test model
      translator = OpenStudio::OSVersion::VersionTranslator.new
      path = OpenStudio::Path.new(File.dirname(__FILE__) + '/' + model_name)
      model = translator.loadModel(path)
      assert(!model.empty?)
      model = model.get
    end

    # get arguments
    arguments = measure.arguments(model)
    argument_map = OpenStudio::Ruleset.convertOSArgumentVectorToMap(arguments)

    # populate argument with specified hash value if specified
    arguments.each do |arg|
      temp_arg_var = arg.clone
      assert(temp_arg_var.setValue(args[arg.name])) if args.key?(arg.name)
      argument_map[arg.name] = temp_arg_var
    end

    # run the measure
    measure.run(model, runner, argument_map)
    result = runner.result

    # show the output
    puts "measure results for #{test_name}"
    show_output(result)

    # assert that it ran correctly
    result_value = 'Success' if result_value.nil?
    assert_equal(result_value, result.value.valueName)

    # check count of warning and info messages
    assert(result.info.size == info_count) unless info_count.nil?
    # TODO: - update tests and un-comment this
    # unless warnings_count.nil? then assert(result.warnings.size == warnings_count) end

    # if 'Fail' passed in make sure at least one error message (while not typical there may be more than one message)
    assert(result.errors.size >= 1) if result_value == 'Fail'

    # save the model to test output directory
    output_file_path = OpenStudio::Path.new(File.dirname(__FILE__) + "/output/#{test_name}_test_output.osm")
    model.save(output_file_path, true)
  end

  def no_test_generic
    args = {}
    args['json'] = 'office_data_2.json'
    # args["maalka_fuel_type"] = "naturalGas"
    args['start_date'] = '2012-01-1'
    args['end_date'] = '2012-12-31'
    apply_measure_to_model(__method__.to_s.gsub('test_', ''), args)
  end

  def test_midrise
    args = {}
    args['json'] = 'pre_1980_midrise.json'
    # args["maalka_fuel_type"] = "naturalGas"
    args['start_date'] = '2012-01-1'
    args['end_date'] = '2012-12-31'
    apply_measure_to_model(__method__.to_s.gsub('test_', ''), args)
  end

  def test_no_units
    args = {}
    args['json'] = 'office_data_2_no_units.json'
    apply_measure_to_model(__method__.to_s.gsub('test_', ''), args, nil, 'Fail')
  end

  def test_no_weather_file_name
    args = {}
    args['json']
    args['json'] = 'office_data_2_no_weather.json'
    apply_measure_to_model(__method__.to_s.gsub('test_', ''), args, nil, 'Fail')
  end
end
