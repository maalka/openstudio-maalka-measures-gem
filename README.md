# Openstudio Maalka Model Articulation Gem

TODO: Add a description of this gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'openstudio-maalka-measures'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install 'openstudio-maalka-measures'

## Tasks

First, install the dependencies with: 

```
bundle install
```

Check all available tasks with:

```
bundle exec rake -T
```

## Tests

Run tests for all measures:

```
bundle exec rake openstudio:test_with_openstudio
```

## Usage

To be filled out later. 

## TODO

- [ ] Remove measures from OpenStudio-Measures to standardize on this location

# Releasing

* Update change log
* Update version in `/lib/openstudio/openstudio-maalka-measures/version.rb`
* Merge down to master
* Release via github
* run `rake release` from master
